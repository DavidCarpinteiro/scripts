function rest_register(url, username, email, password) {
    print('------------------REGISTER------------------')
    var data = JSON.stringify({
        username: username,
        email: email,
        password: password,
        confirmation: password
    });

    response = rest(url, data, 'POST')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_login(url, username, password) {
    print('------------------LOGIN------------------')
    var data = JSON.stringify({
        username: username,
        password: password
    });

    response = rest(url, data, 'POST')

    print(response.status)
    print(response.responseJSON)

    if (response.status === 200) {
        localStorage.setItem('token', response.responseJSON.token)
        localStorage.setItem('session', response.responseJSON.session)
    }

    print('')
}

function rest_info(url, name, zipcode, phoneNumber, nif, imageID, address) {
    print('------------------INFO------------------')
    var data = JSON.stringify({
        name: name,
        zipcode: zipcode,
        phoneNumber: phoneNumber,
        nif: nif,
        imageID: imageID,
        address: address
    });

    response = rest(url, data, 'PUT')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_create_occurrence(url, title, description, type, lat, lng, imageID, lats, lngs) {
    print('------------------CREATE OCC------------------')
    var data = JSON.stringify({
        title: title,
        description: description,
        type: type,
        lat: lat,
        lng: lng,
        imageID: imageID,
        lats: lats,
        lngs: lngs
    });

    response = rest(url, data, 'POST')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_logout(url) {
    print('------------------LOGOUT------------------')

    response = rest(url, {}, 'DELETE')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_list_my(url) {
    print('------------------LIST MY------------------')

    response = rest(url, {}, 'GET')

    print(response.status)
    print(response.responseJSON)


    if (response.status === 200) {
        localStorage.setItem('occ_id', response.responseJSON.data[0].id)
        localStorage.setItem('occ_username', response.responseJSON.data[0].username)

    }

    print('')
}

function rest_vote_occurrence(url, id, username) {
    print('------------------VOTE OCC------------------')
    var data = JSON.stringify({
        id: id,
        username: username
    });

    response = rest(url, data, 'POST')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_remove_vote_occurrence(url, id, username) {
    print('------------------VOTE OCC------------------')
    var data = JSON.stringify({
        id: id,
        username: username
    });

    response = rest(url, data, 'DELETE')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_comment(url, comment, id, username) {
    print('------------------COMMENT OCC------------------')
    var data = JSON.stringify({
        comment: comment,
        id: id,
        username: username
    });

    response = rest(url, data, 'POST')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_list_my_comments(url) {
    print('------------------LIST MY COMMENTS OCC------------------')

    response = rest(url, {}, 'GET')

    print(response.status)
    print(response.responseJSON)

    if (response.status === 200)
        localStorage.setItem('comment_id', response.responseJSON[0].comment_id)

    print('')
}

function rest_delete_comment(url, comment_id) {
    print('------------------DELETE MY COMMENT OCC------------------')
    var data = JSON.stringify({
        comment_id: comment_id
    });

    response = rest(url, data, 'DELETE')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_edit_comment(url, comment, comment_id) {
    print('------------------EDIT MY COMMENT OCC------------------')
    var data = JSON.stringify({
        comment_id: comment_id,
        comment: comment
    });

    response = rest(url, data, 'PUT')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_list_my_votes(url) {
    print('------------------LIST MY VOTES OCC------------------')

    response = rest(url, {}, 'GET')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_my_info(url) {
    print('------------------MY INFO------------------')

    response = rest(url, {}, 'GET')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_list_users(url, password) {
    print('------------------LIST USERS------------------')
    var data = JSON.stringify({
        password: password
    });

    response = rest(url, data, 'POST')

    print(response.status)
    print(response.responseJSON)

    print('')
}

function rest_delete_user_admin(url, password, username) {
    print('------------------DELETE USER------------------')
    var data = JSON.stringify({
        password: password,
        username: username
    });

    response = rest(url, data, 'DELETE')

    print(response.status)
    print(response.responseJSON)

    print('')
}



function rest(url, data, TYPE) {
    return $.ajax({
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        async: false,
        type: TYPE,
        url: url,
        data: data,
        headers: {
            'Authorization': localStorage.getItem('token'),
            'SessionID': localStorage.getItem('session')
        }
    });
}
