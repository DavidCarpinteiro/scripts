let System = {
    out: {
        println: console.log
    }
}

let url = 'https://gaea-8bit.appspot.com/rest/w/'
//let url = 'http://localhost:8080/rest/w/'

let register_url = url + 'user/register'
let login_url = url + 'user/login'
let info_url = url + 'user/add_info'
let logout_url = url + 'user/logout'
let my_info_url = url + 'userInfo/getInfo'

let list_my_votes_url = url + 'userInfo/myVotes'
let list_my_comments_url = url + 'userInfo/myComments'

let create_occ_url = url + 'occurrence/add'
let vote_url = url + 'occurrence/vote'
let remove_vote_url = url + 'occurrence/deleteVote'
let comment_url = url + 'occurrence/comment'
let delete_comment_url = url + 'occurrence/deleteComment'
let edit_comment_url = url + 'occurrence/editComment'

let list_my_url = url + 'occurrenceInfo/myOccurrences'

let list_users_url = url + 'special/listUsers'
let delete_user_url = url + 'special/deleteUser'

function toggleImage() {
    let x = document.getElementById("gandalf");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

$(document).ready(function () {
    toggleImage();
    $('#launch').click(execute)
});

function print(line) {
    console.log(line)
    $('#towrite').append('<p>' + line + '<br></p>');
}

function execute() {
    System.out.println('executing...')

    //registerUsers(10);

    //execute_crash(50);

    testUser();

    testVotes();

    testComments();

    rest_list_users(list_users_url, 'aaAA11')

    /*
    TODO
    Delete
    Change Password
    Edit Occurrence
    Delete Occurrence
    List my Occurrences
    List all Occurrences
    ...
    */

    rest_delete_user_admin(delete_user_url, 'aaAA11');

    toggleImage();
    System.out.println('completed!')
}

function jsonTable() {

  var obj = JSON.parse('{"change_self_email": { "u_basic": true, "u_worker": false, "u_entity": true, "u_admin": true }, "edit_self_profile": { "u_basic": true, "u_worker": true, "u_entity": true, "u_admin": true }, "delete_self_account": { "u_basic": true, "u_worker": false, "u_entity": true, "u_admin": true }, "change_self_password": { "u_basic": true, "u_worker": true, "u_entity": true, "u_admin": true }, "list_self_info": { "u_basic": true, "u_worker": true, "u_entity": true, "u_admin": true }, "list_other_basic_info": { "u_basic": true, "u_worker": true, "u_entity": true, "u_admin": true }, "list_self_comments": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "list_self_votes": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "add_image": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "list_self_points": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "create_new_occurrence": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "edit_self_occurrence": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "delete_self_occurrence": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "vote_occurrence": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "delete_vote": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "comment_occurrence": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "delete_comment": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "edit_comment": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "report_occurrence": { "u_basic": true, "u_worker": true, "u_entity": true, "u_admin": true }, "list_my_occurrences": { "u_basic": true, "u_worker": false, "u_entity": false, "u_admin": true }, "list_self_worker_jobs": { "u_basic": false, "u_worker": true, "u_entity": false, "u_admin": true }, "change_other_occurrence_type": { "u_basic": false, "u_worker": true, "u_entity": false, "u_admin": true }, "mark_occurrence_complete": { "u_basic": false, "u_worker": true, "u_entity": false, "u_admin": true }, "create_worker": { "u_basic": false, "u_worker": false, "u_entity": true, "u_admin": true }, "edit_self_worker": { "u_basic": false, "u_worker": false, "u_entity": true, "u_admin": true }, "delete_self_worker": { "u_basic": false, "u_worker": false, "u_entity": true, "u_admin": true }, "associate_self_occurrence_worker": { "u_basic": false, "u_worker": false, "u_entity": true, "u_admin": true }, "disassociate_self_occurrence_worker": { "u_basic": false, "u_worker": false, "u_entity": true, "u_admin": true }, "confirm_self_job_completion": { "u_basic": false, "u_worker": false, "u_entity": true, "u_admin": true }, "list_self_workers": { "u_basic": false, "u_worker": false, "u_entity": true, "u_admin": true }, "list_self_jobs": { "u_basic": false, "u_worker": false, "u_entity": true, "u_admin": true }, "edit_all_occurrences": { "u_basic": false, "u_worker": false, "u_entity": false, "u_admin": true }, "list_all_occurrences": { "u_basic": false, "u_worker": false, "u_entity": true, "u_admin": true }, "delete_all_occurrences": { "u_basic": false, "u_worker": false, "u_entity": false, "u_admin": true }, "delete_all_users": { "u_basic": false, "u_worker": false, "u_entity": false, "u_admin": true }, "list_all_users": { "u_basic": false, "u_worker": false, "u_entity": false, "u_admin": true }, "verify_role_change": { "u_basic": false, "u_worker": false, "u_entity": false, "u_admin": true }, "delete_all_comments": { "u_basic": false, "u_worker": false, "u_entity": false, "u_admin": true }, "create_new_admin": { "u_basic": false, "u_worker": false, "u_entity": false, "u_admin": true } }');
  //let perm = JSON.parse();
  let basic = {};
  let worker = {};
  let entity = {};
  let admin = {};
  let table = {};
console.log(obj);
  for(let action in obj) {
    basic[action] = obj[action].u_basic;
    worker[action] = obj[action].u_worker;
    entity[action] = obj[action].u_entity;
    admin[action] = obj[action].u_admin;
  }

  console.log(basic);
  table['u_basic'] = basic;
  table['u_worker'] = worker;
  table['u_entity'] = entity;
  table['u_admin'] = admin;
  console.log(table);
  console.log(JSON.stringify(table));

}

function registerUsers(n_users) {
  for (let i = 0; i <= n_users; i++) {
    rest_register(register_url, 'userBasic' + i, i + 'user@gmail.com', 'aaAA11');
  }
}

function testUser() {
  rest_login(login_url, 'userBasic2', 'aaAA11');
  rest_my_info(my_info_url+'?filter=all')
  rest_info(info_url, 'Joao', '1234-567', 987654321, 123456789, '34x5q67er89', 'rua casa numero 1ª porta A');
}

function testVotes() {
  rest_login(login_url, 'userBasic3', 'aaAA11');

  rest_list_my(list_my_url);

  rest_vote_occurrence(vote_url, localStorage.getItem('occ_id'), localStorage.getItem('occ_username'));

  rest_list_my_votes(list_my_votes_url);

  //rest_remove_vote_occurrence(remove_vote_url, localStorage.getItem('occ_id'), localStorage.getItem('occ_username'));

  //rest_list_my_votes(list_my_votes_url);
}

function testComments() {
  rest_login(login_url, 'userBasic3', 'aaAA11');

  rest_list_my(list_my_url);

  rest_comment(comment_url, 'this is a comment', localStorage.getItem('occ_id'), localStorage.getItem('occ_username'));

  rest_list_my_comments(list_my_comments_url);

  rest_edit_comment(edit_comment_url, 'new comment', localStorage.getItem('comment_id'));

  rest_list_my_comments(list_my_comments_url);

  //rest_delete_comment(delete_comment_url, localStorage.getItem('comment_id'));

  //rest_list_my_comments(list_my_comments_url);
}

function execute_crash(number_cicles) {
  let counter = number_cicles;
  let cicle = setInterval(function(){
      print(counter);
      crash(counter);
      counter++;
      if (counter === 50) {
          clearInterval(cicle);
      }
  }, 20000);
}

function getCoord(from, to) {
    return (Math.random() * (to - from) + from);
}

function crash(b) {

  //rest_create_occurrence(create_occ_url, 'AREA', 'desc', 'other', 0, 0, 'sdsad23', [38.665880126,38.66155741,38.658172776], [-9.162923114,-9.168459194,-9.170433299]);

    for (let i = 0; i < 10; i++) {
        let lat = getCoord(42.0, 36.9);
        let lng = getCoord(-9.6, -6.1);
        let t = Math.floor(getCoord(1, 5));

        let type;
        switch (t) {
            case 1:
                type = "forest_cleanup";
                break;
            case 2:
                type = "tree_cutting";
                break;
            case 3:
                type = "garbage_cleanup";
                break;
            case 4:
                type = "other";
                break;
        }

        rest_create_occurrence(create_occ_url, 'TESTOCC'+i*b, 'desc', type, lat, lng, '', [], []);
    }
}
